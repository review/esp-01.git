#ifndef __USER_ESPSWITCH_H__
#define __USER_ESPSWITCH_H__

#include "driver/key.h"
#include "pwm.h"

/* NOTICE---this is for 512KB spi flash.
 * you can change to other sector if you use other size spi flash. */
#define PRIV_PARAM_START_SEC		0x3C
#define PRIV_PARAM_SAVE     0


#define FAN_KEY_NUM            1

#define PWM_CHANNEL_NUM        1
#define FAN_SPEED		       0
#define LIGHT_NONE             1

#define FAN_KEY_0_IO_MUX     PERIPHS_IO_MUX_GPIO0_U
#define FAN_KEY_0_IO_NUM     0
#define FAN_KEY_0_IO_FUNC    FUNC_GPIO0

#define FAN_RELAY_LED_IO_MUX     PERIPHS_IO_MUX_GPIO2_U
#define FAN_RELAY_LED_IO_NUM     2 
#define FAN_RELAY_LED_IO_FUNC    FUNC_GPIO2

#define PWM_0_OUT_IO_MUX		FAN_RELAY_LED_IO_MUX
#define PWM_0_OUT_IO_NUM		FAN_RELAY_LED_IO_NUM
#define PWM_0_OUT_IO_FUNC		FAN_RELAY_LED_IO_FUNC


#define FAN_STATUS_OUTPUT(pin, on)     GPIO_OUTPUT_SET(pin, on)

struct fan_saved_param {
    uint8   status;
	uint32  pwm_period;
	uint32  pwm_duty[PWM_CHANNEL_NUM];
};

void user_fan_init(void);
uint8 user_fan_get_status(void);
void user_fan_set_status(uint8 status, uint32 period, uint32 speed);
void user_fan_close(void);
void user_fan_open(void);
uint32 user_fan_get_period(void);
void user_fan_set_period(uint32 period);
uint32 user_fan_get_duty(uint8 channel);
void user_fan_set_duty(uint32 duty, uint8 channel);

int user_fan_getmethed(char* szOut);
int user_fan_postmethed(char* postdata, char* szOut);
#endif

