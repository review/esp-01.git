/******************************************************************************
 * Copyright 2013-2014 Espressif Systems (Wuxi)
 *
 * FileName: user_fan.c
 *
 * Description: fan demo's function realization
 *
 * Modification history:
 *	 2014/5/1, v1.0 create this file.
*******************************************************************************/
#include "ets_sys.h"
#include "osapi.h"
#include "os_type.h"
#include "mem.h"
#include "user_interface.h"

#include "user_fan.h"
#include "driver/key.h"
#include "cJSON.h"

#if FAN_DEVICE

LOCAL struct keys_param keys;
LOCAL struct single_key_param *single_key[FAN_KEY_NUM];
LOCAL struct fan_saved_param fan_param;

/******************************************************************************
 * FunctionName : user_fan_get_status
 * Description  : get fan's status, 0x00 or 0x01
 * Parameters   : none
 * Returns	  : uint8 - fan's status
*******************************************************************************/
uint8 ICACHE_FLASH_ATTR
user_fan_get_status(void)
{
	return fan_param.status;
}

/******************************************************************************
 * FunctionName : user_fan_set_status
 * Description  : set fan's status, 0x00 or 0x01
 * Parameters   : uint8 - status
 * Returns	  : none
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_fan_set_status(uint8 status, uint32 period, uint32 speed)
{
	//亮度调为0为关机
	if(speed <= 0) {
	   	status = 0;
		speed = 0;
	}
		
	if(speed >= 255)
		speed = 255;

	if (status != fan_param.status) {
		if (status > 1){
			os_printf("error status input[%d]!\n", status);
			return;
		}
		fan_param.status = status;
	}

	//开关机控制
//	LIGHT_BR_STATUS_OUTPUT(LIGHT_BR_RELAY_LED_IO_NUM, status);

	if(status) {
		user_fan_set_period(period);
		user_fan_set_duty(speed, FAN_SPEED);
	
		//使能PWM
		pwm_start();
	}
	else {
		user_fan_set_period(period);
		user_fan_set_duty(0, FAN_SPEED);
		pwm_start();
	}
}

/******************************************************************************
 * FunctionName : user_fan_set_status
 * Description  : set fan's status, 0x00 or 0x01
 * Parameters   : uint8 - status
 * Returns	  : none
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_fan_close()
{
	user_fan_set_status(0, fan_param.pwm_period,
			fan_param.pwm_duty[FAN_SPEED]);
}

/******************************************************************************
 * FunctionName : user_fan_set_status
 * Description  : set fan's status, 0x00 or 0x01
 * Parameters   : uint8 - status
 * Returns	  : none
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_fan_open()
{
	user_fan_set_status(0, fan_param.pwm_period,
			fan_param.pwm_duty[FAN_SPEED]);
}

/******************************************************************************
 * FunctionName : user_fan_get_duty
 * Description  : get duty of each channel
 * Parameters   : uint8 channel : LIGHT_RED/LIGHT_GREEN/LIGHT_BLUE
 * Returns	  : NONE
*******************************************************************************/
uint32 ICACHE_FLASH_ATTR
user_fan_get_duty(uint8 channel)
{
	return fan_param.pwm_duty[channel];
}

/******************************************************************************
 * FunctionName : user_fan_set_duty
 * Description  : set each channel's duty params
 * Parameters   : uint8 duty	: 0 ~ PWM_DEPTH
 *				uint8 channel : LIGHT_RED/LIGHT_GREEN/LIGHT_BLUE
 * Returns	  : NONE
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_fan_set_duty(uint32 speed, uint8 channel)
{
	//计算占空比 0 < speed <= 255
	uint32 duty_tmp = (int)((speed / 255.0) * (fan_param.pwm_period*1000/45));
	os_printf("duty %d pwm_period %d\n", duty_tmp, fan_param.pwm_period);
	os_printf("speed %d pwm_duty %d\n", speed, fan_param.pwm_duty[channel]);
	if (speed != fan_param.pwm_duty[channel]) {
		pwm_set_duty(duty_tmp, channel);

		fan_param.pwm_duty[channel] =
			(int)((pwm_get_duty(channel) /(fan_param.pwm_period*1000/45.0)) * 255);

	}
	os_printf("pwm_duty %d\n", fan_param.pwm_duty[channel]);
}

/******************************************************************************
 * FunctionName : user_fan_get_duty
 * Description  : get duty of each channel
 * Parameters   : uint8 channel : LIGHT_RED/LIGHT_GREEN/LIGHT_BLUE
 * Returns	  : NONE
*******************************************************************************/
uint32 ICACHE_FLASH_ATTR
user_fan_get_period()
{
	return fan_param.pwm_period;
}

/******************************************************************************
 * FunctionName : user_fan_get_duty
 * Description  : get duty of each channel
 * Parameters   : uint8 channel : LIGHT_RED/LIGHT_GREEN/LIGHT_BLUE
 * Returns	  : NONE
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_fan_set_period(uint32 period)
{
	if(period < 1000 || period > 10000)
		period = 1000;

	if(period != fan_param.pwm_period ) {
			pwm_set_period(period);
	 }
	 fan_param.pwm_period = pwm_get_period();
}


/******************************************************************************
 * FunctionName : user_fan_short_press
 * Description  : key's short press function, needed to be installed
 * Parameters   : none
 * Returns	  : none
*******************************************************************************/
LOCAL void ICACHE_FLASH_ATTR
user_fan_short_press(void)
{
	user_fan_set_status((~fan_param.status) & 0x01,
		fan_param.pwm_period, fan_param.pwm_duty[FAN_SPEED]);

	spi_flash_erase_sector(PRIV_PARAM_START_SEC + PRIV_PARAM_SAVE);
	spi_flash_write((PRIV_PARAM_START_SEC + PRIV_PARAM_SAVE) * SPI_FLASH_SEC_SIZE,
				(uint32 *)&fan_param, sizeof(struct fan_saved_param));
}

/******************************************************************************
 * FunctionName : user_fan_long_press
 * Description  : key's long press function, needed to be installed
 * Parameters   : none
 * Returns	  : none
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_fan_long_press(void)
{
	struct softap_config  apconfig;
	wifi_set_opmode(SOFTAP_MODE);
	wifi_softap_get_config(&apconfig);
	os_memset(apconfig.ssid, 0, 32);
	os_memset(apconfig.password, 0, 64);
	os_sprintf(apconfig.ssid,"ESP_%X",system_get_chip_id());
	apconfig.authmode = AUTH_OPEN;
	apconfig.ssid_len = 0;
	apconfig.max_connection = 5;
	wifi_softap_set_config(&apconfig);
	wifi_station_set_reconnect_policy(FALSE);
	wifi_station_set_auto_connect(FALSE);
	wifi_station_disconnect();
}

/******************************************************************************
 * FunctionName : user_fan_getmethed
 * Description  : web get call
 * Parameters   : none
 * Returns	  : none
*******************************************************************************/
int ICACHE_FLASH_ATTR
user_fan_getmethed(char* szOut)
{
	cJSON *root,*fmt; 
	char *out;
	root = cJSON_CreateObject();       
/*	cJSON_AddItemToObject(root, "Response", fmt=cJSON_CreateObject());  by vr7jj 20180509*/
	if(user_fan_get_status()==1){
		cJSON_AddStringToObject(root, "status", "true");
	}else{
		cJSON_AddStringToObject(root, "status", "false");
	}
	cJSON_AddNumberToObject(root,"speed",  user_fan_get_duty(FAN_SPEED));     
	out=cJSON_PrintUnformatted(root);
	cJSON_Delete(root);
	os_printf("1out[%s]\r\n", out);
	os_memcpy(szOut, out, strlen(out));
	os_free(out);
	return 0;
}

/******************************************************************************
 * FunctionName : user_fan_postmethed
 * Description  : web post call
 * Parameters   : none
 * Returns	  : none
*******************************************************************************/
int ICACHE_FLASH_ATTR
user_fan_postmethed(char* postdata, char* szOut)
{
	uint32 speed = 255, period = 1000;
	uint8 fan_status = 0;

	cJSON * t ;
	cJSON * root = cJSON_Parse(postdata);
	if (NULL != root) {
		t = cJSON_GetObjectItem(root, "status");
		if(os_strncmp(t->valuestring, "true", 5) == 0){
			fan_status = 1;
		}else
			fan_status = 0;
		t = cJSON_GetObjectItem(root, "speed");
		speed = t->valueint;
		cJSON_Delete(root);
		user_fan_set_status(fan_status, period, speed);
		user_fan_getmethed(szOut);
	} else {
		os_printf("\r\nparse error!\r\n");
		return -1 ;
	}
	return 0;
}

/******************************************************************************
 * FunctionName : user_fan_init
 * Description  : init fan's key function and relay output
 * Parameters   : none
 * Returns	  : none
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_fan_init(void)
{
	struct fan_saved_param fan_init;
	
	//按键初始化
	single_key[0] = key_init_single(FAN_KEY_0_IO_NUM, FAN_KEY_0_IO_MUX, FAN_KEY_0_IO_FUNC,
			   user_fan_long_press, user_fan_short_press);

	keys.key_num = FAN_KEY_NUM;
	keys.single_key = single_key;
	key_init(&keys);

	//PWM初始化
	spi_flash_read((PRIV_PARAM_START_SEC + PRIV_PARAM_SAVE) * SPI_FLASH_SEC_SIZE,
				(uint32 *)&fan_init, sizeof(struct fan_saved_param));

	if(fan_init.pwm_period>10000 || fan_init.pwm_period <1000){
			fan_init.pwm_period = 1000;
	}

	// no used SPI Flash
	if (fan_init.status == 0xff) {
		fan_init.status = 1;
		fan_init.pwm_period = 1000;
		//最大占空比peroid*1000/45
		fan_init.pwm_duty[FAN_SPEED] = 255;
	}

	uint32 io_info[][3] = {
			{PWM_0_OUT_IO_MUX,PWM_0_OUT_IO_FUNC,PWM_0_OUT_IO_NUM}
						  };

	uint32 pwm_duty_init[PWM_CHANNEL_NUM] = {0};
	pwm_init(fan_init.pwm_period,  pwm_duty_init, PWM_CHANNEL_NUM, io_info);

	user_fan_set_status(fan_init.status,
			fan_init.pwm_period, fan_init.pwm_duty[FAN_SPEED]);
}
#endif

