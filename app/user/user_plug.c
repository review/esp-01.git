/******************************************************************************
 * Copyright 2013-2014 Espressif Systems (Wuxi)
 *
 * FileName: user_plug.c
 *
 * Description: plug demo's function realization
 *
 * Modification history:
 *	 2014/5/1, v1.0 create this file.
*******************************************************************************/
#include "ets_sys.h"
#include "osapi.h"
#include "os_type.h"
#include "mem.h"
#include "user_interface.h"

#include "user_plug.h"
#include "driver/key.h"
#include "cJSON.h"

#if PLUG_DEVICE

LOCAL struct keys_param keys;
LOCAL struct single_key_param *single_key[PLUG_KEY_NUM];
LOCAL struct plug_saved_param plug_param;

/******************************************************************************
 * FunctionName : user_plug_get_status
 * Description  : get plug's status, 0x00 or 0x01
 * Parameters   : none
 * Returns	  : uint8 - plug's status
*******************************************************************************/
uint8 ICACHE_FLASH_ATTR
user_plug_get_status(void)
{
	return plug_param.status;
}

/******************************************************************************
 * FunctionName : user_plug_set_status
 * Description  : set plug's status, 0x00 or 0x01
 * Parameters   : uint8 - status
 * Returns	  : none
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_plug_set_status(uint8 status)
{
	if (status != plug_param.status) {
		if (status > 1) {
			os_printf("error status input!\n");
			return;
		}

		plug_param.status = status;
		PLUG_STATUS_OUTPUT(PLUG_RELAY_LED_IO_NUM, status);
	}
}

/******************************************************************************
 * FunctionName : user_plug_short_press
 * Description  : key's short press function, needed to be installed
 * Parameters   : none
 * Returns	  : none
*******************************************************************************/
LOCAL void ICACHE_FLASH_ATTR
user_plug_short_press(void)
{
	user_plug_set_status((~plug_param.status) & 0x01);
}

/******************************************************************************
 * FunctionName : user_long_press
 * Description  : key's long press function, needed to be installed
 * Parameters   : none
 * Returns	  : none
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_plug_long_press(void)
{
	struct softap_config  apconfig;
	wifi_set_opmode(SOFTAP_MODE);
	wifi_softap_get_config(&apconfig);
	os_memset(apconfig.ssid, 0, 32);
	os_memset(apconfig.password, 0, 64);
	os_sprintf(apconfig.ssid,"ESP_%X",system_get_chip_id());
	apconfig.authmode = AUTH_OPEN;
	apconfig.ssid_len = 0;
	apconfig.max_connection = 5;
	wifi_softap_set_config(&apconfig);
	wifi_station_set_reconnect_policy(FALSE);
	wifi_station_set_auto_connect(FALSE);
	wifi_station_disconnect();
}

/******************************************************************************
 * FunctionName : user_plug_init
 * Description  : init plug's key function and relay output
 * Parameters   : none
 * Returns	  : none
*******************************************************************************/
uint8 ICACHE_FLASH_ATTR
user_plug_getmethed(char* szOut)
{
	cJSON * root = cJSON_CreateObject();
	cJSON * status = cJSON_CreateObject();
//	cJSON_AddNumberToObject(status, "status", user_plug_get_status());
	if(user_plug_get_status()==1){
		cJSON_AddStringToObject(status, "status", "true");
	}else{
		cJSON_AddStringToObject(status, "status", "false");
	}
	cJSON_AddItemToObject(root, "Response", status);
	char * out=cJSON_PrintUnformatted(root);
	cJSON_Delete(root);
	os_printf("out[%s]\r\n", out);
	os_memcpy(szOut, out, strlen(out));
	os_free(out);
	return 0;
}

/******************************************************************************
 * FunctionName : user_plug_init
 * Description  : init plug's key function and relay output
 * Parameters   : none
 * Returns	  : none
*******************************************************************************/
uint8 ICACHE_FLASH_ATTR
user_plug_postmethed(char* postdata, char* szOut)
{
	cJSON * t ;
	cJSON * root = cJSON_Parse(postdata);
	if (NULL != root) {
		t = cJSON_GetObjectItem(root, "Response");
		if(NULL != t){
			t = cJSON_GetObjectItem(t, "status");
			if(os_strncmp(t->valuestring, "true", 5) == 0){
				user_plug_set_status(1);
			}else if(os_strncmp(t->valuestring, "false", 5)==0){
				user_plug_set_status(0);
			}
			else
				;
		}
		cJSON_Delete(root);
		user_plug_getmethed(szOut);
	} else {
		os_printf("\r\nparse error!\r\n");
		return -1 ;
	}
	return 0;
}

/******************************************************************************
 * FunctionName : user_plug_init
 * Description  : init plug's key function and relay output
 * Parameters   : none
 * Returns	  : none
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_plug_init(void)
{
	spi_flash_read((PRIV_PARAM_START_SEC + PRIV_PARAM_SAVE) * SPI_FLASH_SEC_SIZE,
				(uint32 *)&plug_param, sizeof(struct plug_saved_param));

	single_key[0] = key_init_single(PLUG_KEY_0_IO_NUM, PLUG_KEY_0_IO_MUX, PLUG_KEY_0_IO_FUNC,
		user_plug_long_press, user_plug_short_press);
	keys.key_num = PLUG_KEY_NUM;
	keys.single_key = single_key;
	key_init(&keys);

	PIN_FUNC_SELECT(PLUG_RELAY_LED_IO_MUX, PLUG_RELAY_LED_IO_FUNC);

	// no used SPI Flash
/*	if (plug_param.status == 0xff) {
		plug_param.status = 1;
	} 20180317Ĭ��Ϊ��
*/
	plug_param.status = 1;

	PLUG_STATUS_OUTPUT(PLUG_RELAY_LED_IO_NUM, plug_param.status);
}
#endif

