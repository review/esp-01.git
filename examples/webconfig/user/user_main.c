/******************************************************************************
 * Copyright 2013-2014 Espressif Systems (Wuxi)
 *
 * FileName: user_main.c
 *
 * Description: entry file of user application
 *
 * Modification history:
 *     2014/1/1, v1.0 create this file.
*******************************************************************************/
#include "ets_sys.h"
#include "osapi.h"

#include "user_interface.h"
#include "user_webconfig.h"

#include "driver/uart.h"
#include "driver/key.h"

#define PLUG_KEY_NUM          1
#define PLUG_KEY_0_IO_MUX     PERIPHS_IO_MUX_GPIO0_U
#define PLUG_KEY_0_IO_NUM     0
#define PLUG_KEY_0_IO_FUNC    FUNC_GPIO0

LOCAL struct plug_saved_param plug_param;
LOCAL struct keys_param keys;
LOCAL struct single_key_param *single_key[PLUG_KEY_NUM];

void user_rf_pre_init(void)
{
}


LOCAL void ICACHE_FLASH_ATTR
user_plug_short_press(void)
{
	os_printf("you short press key\r\n");
}

LOCAL void ICACHE_FLASH_ATTR
user_plug_long_press(void)
{
	os_printf("you long press key\r\n");
	user_webserver_init(SERVER_PORT);
}

/******************************************************************************
 * FunctionName : user_init
 * Description  : entry of user application, init user function here
 * Parameters   : none
 * Returns      : none
*******************************************************************************/
void user_init(void)
{
	struct station_config stconfig;

	uart_init(BIT_RATE_115200, BIT_RATE_115200);
    os_printf("SDK version:%s\n", system_get_sdk_version());

	single_key[0] = key_init_single(PLUG_KEY_0_IO_NUM, PLUG_KEY_0_IO_MUX, PLUG_KEY_0_IO_FUNC,
                                user_plug_long_press, user_plug_short_press);

	keys.key_num = PLUG_KEY_NUM;
	keys.single_key = single_key;
	key_init(&keys);

	os_memset(&stconfig, 0, sizeof(struct station_config));
	wifi_station_get_config_default(&stconfig);
	if(os_strlen(stconfig.ssid)!=0){
		wifi_set_opmode(STATION_MODE);
		wifi_station_set_reconnect_policy(TRUE);
		wifi_station_set_auto_connect(TRUE);
	}
	else{
    	user_webserver_init(SERVER_PORT);
	}
}

