/******************************************************************************
 * Copyright 2013-2014 Espressif Systems (Wuxi)
 *
 * FileName: user_light_br.c
 *
 * Description: light_br demo's function realization
 *
 * Modification history:
 *     2014/5/1, v1.0 create this file.
*******************************************************************************/
#include "ets_sys.h"
#include "osapi.h"
#include "os_type.h"
#include "mem.h"
#include "user_interface.h"

#include "user_light_br.h"

#include "user_wifi.h"

#if LIGHT_BR_DEVICE

LOCAL struct light_br_saved_param light_br_param;
LOCAL struct keys_param keys;
LOCAL struct single_key_param *single_key[LIGHT_BR_KEY_NUM];

/******************************************************************************
 * FunctionName : user_light_br_get_status
 * Description  : get light_br's status, 0x00 or 0x01
 * Parameters   : none
 * Returns      : uint8 - light_br's status
*******************************************************************************/
uint8 ICACHE_FLASH_ATTR
user_light_br_get_status(void)
{
    return light_br_param.status;
}

/******************************************************************************
 * FunctionName : user_light_br_set_status
 * Description  : set light_br's status, 0x00 or 0x01
 * Parameters   : uint8 - status
 * Returns      : none
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_light_br_set_status(uint8 status, uint32 period, uint32 brightness)
{
	//亮度调为0为关机
	if(brightness <= 0) {
       	status = 0;
		brightness = 0;
	}
		
	if(brightness > 255)
		brightness = 255;

    if (status != light_br_param.status) {
        if (status > 1){
            os_printf("error status input[%d]!\n", status);
            return;
        }
        light_br_param.status = status;
    }

	//开关机控制
//	LIGHT_BR_STATUS_OUTPUT(LIGHT_BR_RELAY_LED_IO_NUM, status);

	if(status) {
		user_light_br_set_period(period);
		user_light_br_set_duty(brightness, LIGHT_BRIGHTNESS);
	
		//使能PWM
		pwm_start();
	}
	else {
		user_light_br_set_period(period);
		user_light_br_set_duty(0, LIGHT_BRIGHTNESS);
		pwm_start();
	}
}

/******************************************************************************
 * FunctionName : user_light_br_set_status
 * Description  : set light_br's status, 0x00 or 0x01
 * Parameters   : uint8 - status
 * Returns      : none
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_light_br_close()
{
	user_light_br_set_status(0, light_br_param.pwm_period,
			light_br_param.pwm_duty[LIGHT_BRIGHTNESS]);
}

/******************************************************************************
 * FunctionName : user_light_br_set_status
 * Description  : set light_br's status, 0x00 or 0x01
 * Parameters   : uint8 - status
 * Returns      : none
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_light_br_open()
{
	user_light_br_set_status(0, light_br_param.pwm_period,
			light_br_param.pwm_duty[LIGHT_BRIGHTNESS]);
}

/******************************************************************************
 * FunctionName : user_light_get_duty
 * Description  : get duty of each channel
 * Parameters   : uint8 channel : LIGHT_RED/LIGHT_GREEN/LIGHT_BLUE
 * Returns      : NONE
*******************************************************************************/
uint32 ICACHE_FLASH_ATTR
user_light_br_get_duty(uint8 channel)
{
    return light_br_param.pwm_duty[channel];
}

/******************************************************************************
 * FunctionName : user_light_br_set_duty
 * Description  : set each channel's duty params
 * Parameters   : uint8 duty    : 0 ~ PWM_DEPTH
 *                uint8 channel : LIGHT_RED/LIGHT_GREEN/LIGHT_BLUE
 * Returns      : NONE
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_light_br_set_duty(uint32 brightness, uint8 channel)
{
	//计算占空比 0 < brightness <= 255
	uint32 duty_tmp = (int)((brightness / 255.0) * (light_br_param.pwm_period*1000/45));
	os_printf("duty %d pwm_period %d\n", duty_tmp, light_br_param.pwm_period);
	os_printf("brightness %d pwm_duty %d\n", brightness, light_br_param.pwm_duty[channel]);
    if (brightness != light_br_param.pwm_duty[channel]) {
        pwm_set_duty(duty_tmp, channel);

        light_br_param.pwm_duty[channel] =
			(int)((pwm_get_duty(channel) /(light_br_param.pwm_period*1000/45.0)) * 255);

    }
	os_printf("pwm_duty %d\n", light_br_param.pwm_duty[channel]);
}

/******************************************************************************
 * FunctionName : user_light_get_duty
 * Description  : get duty of each channel
 * Parameters   : uint8 channel : LIGHT_RED/LIGHT_GREEN/LIGHT_BLUE
 * Returns      : NONE
*******************************************************************************/
uint32 ICACHE_FLASH_ATTR
user_light_br_get_period()
{
    return light_br_param.pwm_period;
}

/******************************************************************************
 * FunctionName : user_light_get_duty
 * Description  : get duty of each channel
 * Parameters   : uint8 channel : LIGHT_RED/LIGHT_GREEN/LIGHT_BLUE
 * Returns      : NONE
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_light_br_set_period(uint32 period)
{
	if(period < 1000 || period > 10000)
        period = 1000;

    if(period != light_br_param.pwm_period ) {
            pwm_set_period(period);
     }
     light_br_param.pwm_period = pwm_get_period();
}


/******************************************************************************
 * FunctionName : user_light_br_short_press
 * Description  : key's short press function, needed to be installed
 * Parameters   : none
 * Returns      : none
*******************************************************************************/
LOCAL void ICACHE_FLASH_ATTR
user_light_br_short_press(void)
{
    user_light_br_set_status((~light_br_param.status) & 0x01,
		light_br_param.pwm_period, light_br_param.pwm_duty[LIGHT_BRIGHTNESS]);

    spi_flash_erase_sector(PRIV_PARAM_START_SEC + PRIV_PARAM_SAVE);
    spi_flash_write((PRIV_PARAM_START_SEC + PRIV_PARAM_SAVE) * SPI_FLASH_SEC_SIZE,
        		(uint32 *)&light_br_param, sizeof(struct light_br_saved_param));
}

/******************************************************************************
 * FunctionName : user_light_br_long_press
 * Description  : key's long press function, needed to be installed
 * Parameters   : none
 * Returns      : none
*******************************************************************************/
LOCAL void ICACHE_FLASH_ATTR
user_light_br_long_press(void)
{
	user_esp_platform_set_active(0);
	wifi_connect();
}


/******************************************************************************
 * FunctionName : user_light_br_init
 * Description  : init light_br's key function and relay output
 * Parameters   : none
 * Returns      : none
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_light_br_init(void)
{
	struct light_br_saved_param light_br_init;
	
	//按键初始化
    single_key[0] = key_init_single(LIGHT_BR_KEY_0_IO_NUM, LIGHT_BR_KEY_0_IO_MUX, LIGHT_BR_KEY_0_IO_FUNC,
                                    user_light_br_long_press, user_light_br_short_press);
    keys.key_num = LIGHT_BR_KEY_NUM;
    keys.single_key = single_key;
    key_init(&keys);

	//PWM初始化
    spi_flash_read((PRIV_PARAM_START_SEC + PRIV_PARAM_SAVE) * SPI_FLASH_SEC_SIZE,
        		(uint32 *)&light_br_init, sizeof(struct light_br_saved_param));

    if(light_br_init.pwm_period>10000 || light_br_init.pwm_period <1000){
            light_br_init.pwm_period = 1000;
    }

    // no used SPI Flash
    if (light_br_init.status == 0xff) {
        light_br_init.status = 1;
        light_br_init.pwm_period = 1000;
		//最大占空比peroid*1000/45
        light_br_init.pwm_duty[LIGHT_BRIGHTNESS] = 255;
    }

//    uint32 io_info[][3] = {
//			{PWM_0_OUT_IO_MUX,PWM_0_OUT_IO_FUNC,PWM_0_OUT_IO_NUM},
//            {PWM_1_OUT_IO_MUX,PWM_1_OUT_IO_FUNC,PWM_1_OUT_IO_NUM},
//                          };
    uint32 io_info[][3] = {
			{PWM_0_OUT_IO_MUX,PWM_0_OUT_IO_FUNC,PWM_0_OUT_IO_NUM}
                          };

    uint32 pwm_duty_init[PWM_CHANNEL] = {0};
    pwm_init(light_br_init.pwm_period,  pwm_duty_init, PWM_CHANNEL, io_info);

	//TODO 使能PWM
	user_light_br_set_status(light_br_init.status,
			light_br_init.pwm_period, light_br_init.pwm_duty[LIGHT_BRIGHTNESS]);
}
#endif

