#ifndef __USER_ESPSWITCH_H__
#define __USER_ESPSWITCH_H__

#include "driver/key.h"
#include "pwm.h"

/* NOTICE---this is for 512KB spi flash.
 * you can change to other sector if you use other size spi flash. */
#define PRIV_PARAM_START_SEC		0x3C

#define PRIV_PARAM_SAVE     0

#define LIGHT_BR_KEY_NUM            1

#define PWM_CHANNEL            1
#define LIGHT_BRIGHTNESS       0
#define LIGHT_NONE             1

#define LIGHT_BR_KEY_0_IO_MUX     PERIPHS_IO_MUX_GPIO0_U
#define LIGHT_BR_KEY_0_IO_NUM     0
#define LIGHT_BR_KEY_0_IO_FUNC    FUNC_GPIO0

#define LIGHT_BR_RELAY_LED_IO_MUX     PERIPHS_IO_MUX_GPIO2_U
#define LIGHT_BR_RELAY_LED_IO_NUM     2 
#define LIGHT_BR_RELAY_LED_IO_FUNC    FUNC_GPIO2

#define PWM_0_OUT_IO_MUX		LIGHT_BR_RELAY_LED_IO_MUX
#define PWM_0_OUT_IO_NUM		LIGHT_BR_RELAY_LED_IO_NUM
#define PWM_0_OUT_IO_FUNC		LIGHT_BR_RELAY_LED_IO_FUNC

/*
#define PWM_1_OUT_IO_MUX PERIPHS_IO_MUX_MTDO_U
#define PWM_1_OUT_IO_NUM 15
#define PWM_1_OUT_IO_FUNC  FUNC_GPIO15
*/


#define LIGHT_BR_STATUS_OUTPUT(pin, on)     GPIO_OUTPUT_SET(pin, on)

struct light_br_saved_param {
    uint8   status;
	uint32  pwm_period;
	uint32  pwm_duty[PWM_CHANNEL];
};

void user_light_br_init(void);
uint8 user_light_br_get_status(void);
void user_light_br_set_status(uint8 status, uint32 period, uint32 brightness);
void user_light_br_close(void);
void user_light_br_open(void);
uint32 user_light_br_get_period(void);
void user_light_br_set_period(uint32 period);
uint32 user_light_br_get_duty(uint8 channel);
void user_light_br_set_duty(uint32 duty, uint8 channel);
uint32 user_light_get_period(void);
void user_light_set_period(uint32 period);

#endif

